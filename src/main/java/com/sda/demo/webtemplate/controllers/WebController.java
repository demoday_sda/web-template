package com.sda.demo.webtemplate.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.websocket.server.PathParam;

@Controller
public class WebController {

    @RequestMapping("/")
    public String hello(@RequestParam(name = "kto", required = false) String kto, Model model) {

        if (kto != null) {
            model.addAttribute("kto", kto);
        }

        return "hello";
    }
}
